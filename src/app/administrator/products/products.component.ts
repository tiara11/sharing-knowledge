import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  dataProducts : any
  // columTable = ['actor_id', 'first_name', 'last_name', 'Update', 'Delete']
  // titlePage = "ACTOR"
  constructor(
    private productsService : ProductsService
  ) {}

  ngOnInit(): void {
    this.getDataProducts()
  }
  getDataProducts(){
    this.productsService.getDataProducts().subscribe(res =>{
      this.dataProducts = res
      console.log(res)

    })

  }

}
