import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { Route, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { SettingsComponent } from './settings/settings.component';


const routes : Routes = [
  {
    path: '', component:MainComponent,
    children:[
      {
        path:'',
        redirectTo:'/admin/home',
        pathMatch:'full'
      },
      {
        path:'home',component:HomeComponent
      },
      {
        path:'list-products',component:ProductsComponent
      },
      {
        path:'settings',component:SettingsComponent
      }
    ]
  }
]

@NgModule({
  declarations: [
    MainComponent,
    HomeComponent,
    ProductsComponent,
    SettingsComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    
    RouterModule.forChild(routes)
  ]
})
export class AdministratorModule { }
